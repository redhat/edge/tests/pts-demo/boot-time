#!/usr/bin/env python3
import sys
import argparse
import json
import webbrowser
from pathlib import Path
import gspread
from googleapiclient.discovery import build
from google.oauth2 import service_account
from google.oauth2 import credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


def parse_args():
    parser = argparse.ArgumentParser(
            description='Generate a gsheet for boot-time results'
            )

    parser.add_argument(
            'stat_json_file',
            # nargs='+',
            help='Output from calcstats.py')

    parser.add_argument(
            '-e',
            '--email',
            help='E-mail address to share gsheet')

    parser.add_argument(
            '-c',
            '--cred-path',
            nargs='?',
            default="credentials.json",
            help='Path to credentials for Google API. Defaults to credentials.json.'),

    parser.add_argument(
            '-s',
            '--service-account',
            action="store_true",
            help='Set credential type to service account, otherwise defaults to oauth.',
            required=False,
            )
    return parser.parse_args()

def transfer_ownership(gsheet,email):
    drive_creds = service_account.Credentials.from_service_account_file(filename='credentials.json', scopes=['https://www.googleapis.com/auth/drive'])
    drive_service = build('drive', 'v3', credentials=drive_creds)
    transfer_body = {
        "role": "owner",
        "transferOwnership": True,
        "type": "user",
        "emailAddress": email,
    }
    response = drive_service.permissions().create(
            fileId=gsheet.id,
            body=transfer_body,
            transferOwnership=True,
            supportsAllDrives=True,
            ).execute()


def generate_sheet(test_name, hostname, all_stats, header, system_config, show_headers=True):
    sheet = []

    sheet.append(["====================================="])
    sheet.append(['Test', test_name])
    sheet.append(['Machine', hostname])
    sheet.extend(system_config)
    sheet.append(["-------------------------------------"])

    for key, stats in all_stats.items():
        if not stats:
            continue

        values = [[stat[key] for key in header] for stat in stats]

        # Create row
        row = [[key.upper()]]
        if show_headers:
            row.append(header)
        row.extend(values)

        sheet.extend(row)

    return sheet

def google_authenticate(scopes, cred_path, is_service_account = False):
    # Use service account for CI
    if is_service_account:
        creds = service_account.Credentials.from_service_account_file(filename=cred_path, scopes=scopes)
    # Otherwise oauth
    else:
        authorized_user_file_path = 'boot-time_token.json'
        # Check if this was previous authoriazed
        if Path(authorized_user_file_path).exists():
            creds = credentials.Credentials.from_authorized_user_file(authorized_user_file_path)
            if creds.expired:
                print("Refreshing token...")
                creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(cred_path, scopes)
            creds = flow.run_local_server(port=0)
            Path(authorized_user_file_path).write_text(creds.to_json())

    return gspread.authorize(creds)


def main(argv):
    # Parse args
    args = parse_args()
    stat_json_file = args.stat_json_file
    email = args.email
    cred_path = args.cred_path
    is_service_account = args.service_account


    all_tests = json.loads(Path(stat_json_file).read_text('UTF-8'))
    # all_stats = all_stats[0]


    scopes = ['https://www.googleapis.com/auth/spreadsheets', 'https://www.googleapis.com/auth/drive']
    gcreds = google_authenticate(scopes, cred_path, is_service_account)

    print("Successfully authorized...")

    # Shared folder setup with P&S team that has access
    gshared_folder_id = "1OcwOQw2YXBlX2PHr0INkO9l7GUbW9ldK"

    print("Creating gsheet...")
    gsheet = gcreds.create(
            'Automotive Boot Time Test - ' + stat_json_file.rstrip('.json'),
            gshared_folder_id)

    # Setup the two worksheets being used. Wanted to do a sheet per result but ends up being complicated
    # if there are naming conflicts
    # - Raw data with all the fields
    worksheet_raw = gsheet.get_worksheet(0)
    worksheet_raw.update_title(f'Boot time - Raw')
    worksheet_raw_fields = ['name',  'mean', 'std_dev', 'percent_sd', 'samples']
    # - Mean only to make it easier to copy and paste
    worksheet_mean = gsheet.add_worksheet(title=f'Boot time - Mean only', rows=0, cols=0)
    worksheet_mean_fields = ['name',  'mean']

    for test in all_tests:

        try:

            # Make sure people get access, mainly useful when using service account
            if is_service_account:
                gsheet.share('acalhoun-directs@redhat.com', perm_type='group', role='writer', notify=False,)
                if email:
                    gsheet.share(email, perm_type='user', role='writer')

            hostname = test['test_config']['hostname']
            system_config = test['system_config']
            system_config['installed_rpms'] = str(system_config['installed_rpms'])
            # Delete the additional QC version info
            if 'qc_firmware_ver_info' in system_config:
                del system_config['qc_firmware_ver_info']

            system_config_list = [[key, system_config[key]] for key in system_config.keys()]

            # Convert the output from calc_stat.spy into the first sheet
            test_description = test["test_config"]["description"]
            test_statistics = test["test_statistics"]

            worksheet_raw.append_rows(
                    generate_sheet(
                        test_description,
                        hostname,
                        test_statistics,
                        worksheet_raw_fields,
                        system_config_list
                        )
                    )

            # Create a sheet that only has the mean values. Optimized for copy/pasting in reports
            worksheet_mean.append_rows(
                    generate_sheet(
                        test_description,
                        hostname,
                        test_statistics,
                        worksheet_mean_fields,
                        system_config_list,
                        show_headers=False
                        )
                    )

        except Exception as e:
            gcreds.del_spreadsheet(gsheet.id)
            raise e

    webbrowser.open_new_tab(gsheet.url)
    print(f'URL: {gsheet.url}')

if __name__ == '__main__':
    main(sys.argv)
