#!/bin/bash
set -e

if [ $# -lt 3 ]; then
  echo "Usage: $0 <username> <password> <path to json results> [TEST]"
  exit 1
fi

# Collect the bearer token from the Horreum Keycloak server
TOKEN=$(curl --fail-with-body -k \
https://horreum-keycloak.corp.redhat.com/realms/horreum/protocol/openid-connect/token \
-d "username=$1" --data-urlencode "password=$2" \
-d 'grant_type=password' -d 'client_id=horreum-ui' | jq -r .access_token)
# Define the target Test for your Run upload
# Dfeault to boot-time-verbose unless otherwise specified
TEST='boot-time-verbose'
if [ -n "$4" ]; then
  TEST="$4"
fi

# Define the start and end times for your Run.
# We capture this data from the run output using jsonpath.
START='$.start_time'
STOP='$.end_time'
# Define the team that has ownership of the Run
OWNER='rhivos-perf-team'
# Define the access permissions for the Run
ACCESS='PUBLIC'
# Push the JSON document up to Horreum.
# The path to the document is passed to this script as an argument.
curl --fail-with-body -k 'https://horreum.corp.redhat.com/api/run/data?test='$TEST'&start='$START'&stop='$STOP'&owner='$OWNER'&access='$ACCESS -X POST -H 'content-type: application/json' -H 'Authorization: Bearer '$TOKEN -d @$3
