# BootTime
Collection of scripts useful to investigate linux boot-time and a CI pipeline to execute them
on Qualcomm Ride4 hardware.

## Pipeline

The GitLab CI pipeline excutes an end to end test on hardware specified by IP address

### Pipeline options

To execute pipeline, go to [Build > Pipeliens > Run pipelines](https://gitlab.com/redhat/edge/tests/perfscale/boot-time/-/pipelines/new).

By default, the pipeline executes the boot-time test on `SUT_IP` and assumes the hardware is all setup accordingly.

#### Provisoning a board with a RHIVOS release

The pipeline can flash a Ride4 board with a RHIVOS release prior to testing with the following variable:
- `PROVISION_RELEASE`: Set the release name for RHIVOS. Only supports ER3 and above.

#### Kernel Flashing options

The following variables add ability to flash a new kernel on the System Under Test (SUT). Currently only
supports Qualcomm Ride4:
- `KERNEL_ABOOT_PATH`: Path to an aboot file on the SUT to flash.
- `KERNEL_RPM_PATH`: Path to a folder on the SUT that has kernel RPMS to install.
- `KERNEL_RPM_DOWNLOAD`: A web URL that contains kernel RPMs to download. Must be able to list the directory contents.

## Scripts

### Dependencies

Dependencies are defined in the [`Pipfile`](Pipfile) and `pipenv` can be used for a virtual environment.
```
pip3 install pipenv
pipenv install
```

To enter the virtual environment, run:
```
pipenv shell
```

### `sut_boottest.py`

This is the primary script used to orchestrate the boot time tests. The script uses `ssh` to connect to the system under test (SUT). Output is generated as a JSON document. The `-s, --samples` argument defines how many reboots will be performed by the script.

The script requires two positional input parameters -- the hostname and IP address of the SUT. The hostname is actually an arbitrary string, but because we cannot confidently collect a "friendly" hostname from the SUT itself, the convention is to provide its hostname to the input parameter. All other parameters are optional and/or have default values (see `sut_boottest.py --help`):
```
sut_boottest.py <hostname> <IP address>
```

#### Test phases
1. Collects and generates metadata for the SUT (run once, not per sample)
2. Runs reboot samples (per `-s, --samples` argument)
    - Reboots the remote SUT and waits for readiness
    - Collects boot time metrics and system logs using `systemd-analyze` and `dmesg`
3. Writes schema-defined JSON file; see [`example_output.json`](example_output.json) and [`boottest-schema.json`](boottest-schema.json)

#### Verbose log capture
The optional `-v , --verbose` flag collects all dmesg and user-space systemd logs and generates an interactive chart for each sample. The chart is created as a self-contained HTML/JavaScript file alongside the primary JSON output file. **In most cases, it may make sense to only use verbose output with a single sample (`-s 1`).**

*Note: Verbose output currently requires a local `systemd-analyze` command version >=253 and passwordless SSH connectivity to the SUT. Remote systemd-analyze is unfortunately much slower that running the command over SSH, but RHEL before 9.4 does not have the required feature from `systemd-analyze` to generate the plot data.*

The interactive chart, powered by [Bokeh](https://bokeh.org/), can be extremely powerful in visualizing boot time activities. Messages from systemd and dmesg are time-aligned so that both are visible together in sequence. The Y-axis represents the sequence of the log messages, and the X-axis represents the boot time, with each log message represented as a bar that shows both the start time and duration of the logged item. Items in blue represent dmesg logs and items in red represent systemd logs. This display is very similar to what is reported by the `systemd-analyze plot` command, but significantly more powerful.

The chart can be zoomed and panned in several ways through the Bokeh UI that is provided. Hovering your mouse over any item will provide you with information about the log message and its timing information.

*Note: Currently any log item that reports a 0 time duration will be visible on the chart, but the mouse hover will not work.*

Verbose output also has several user-tunable options. Please see `sut_boottest.py --help` for more information. One key tunable option, `--kpi-re-pattern`, is a regular expression pattern for matching logs to highlight (green bar and font, and log message displayed).

![example of Bokeh verbose log chart](bokeh_chart_example.png)

### `bokeh_chart.py`
This is simply the companion script to `sut_boottest.py` that generates the interactive Bokeh chart from the verbose log capture.

### `push_to_horreum.sh`
TODO

### `calcstats.py`
post processes sut_boottest.py JSON file
* calculates run-to-run variance stats for:
* systemd-analyze by parsing 'sa_time' JSON object
* systemd-analyze by parsing 'sa_blame' JSON object
* DMESG
    * 'link is up' message and timestamp
    * initramfs
    * early service
    * early clock ticks

### `gsheets.py`
Takes the JSON output of calcstats.py and sut_boottest.py and create a
Google Sheet

### `systemd_unitfile`
Assists with measurement of custom systemd-unitfile timings
* see README.md in directory

### `Sandbox`
Collection of scripts created during development of Boot-time automation
* reboot_test.py: instruments reboot of remote system under test
* ex_channel.py: paramiko ssh example
* rcv_exit_status.py: illustrates blocking with paramiko exec cmds
* sa_json.py : produces json from both 'time' and 'blame' results
NOTE: requires 'pip3.6 import distro'

### `VMs`
Two scripts which work together to report on boot-time stats for a VM
* start_vm_boot.sh: execute on HOST. You need to add cmdline to start your VM
* print_boottime.sh: execute in VM, edit $HOSTNAME. Prints boot-time metrics (see sample.txt)
* sample.txt: example of output from 'print_boottimek.sh'
