#!/bin/bash

# Runs commands for systems with abootimg
abootimg_cmd() {
	# Need to find out the new size by having it initially fail the first attempt. the `-u` command
	# does not auto resize.
	SIZE=$(abootimg -u /boot/aboot-$1.img -r /boot/initramfs-$1.img 2>&1 | awk -F'[( ]' '/updated is too big for the Boot Image/ {print $11}')

	if [ -z "$SIZE" ]; then
		abootimg -u "/boot/aboot-$1.img" -r "/boot/initramfs-$1.img" -c "bootsize=$SIZE"
	fi
}

# Runs commands for
mkbootimg_cmd() {
	OUTPUT_DIR=$(mktemp -d)
	# Renaming the original so mkbootimg can use the name when it outputs the image
	# so that it keeps the same functionality as abootimg -u
	mv /boot/aboot-$1{,-orig}.img
	# Get the args from the existing image
	declare -a ARGS="($(unpack_bootimg --boot_img "/boot/aboot-$1-orig.img" --out "$OUTPUT_DIR/out" --format=mkbootimg))"
	# Override the ramdisk for the one in /boot/
	mkbootimg "${ARGS[@]}" --ramdisk /boot/initramfs-$1.img --output "/boot/aboot-$1.img"
}
echo "[Unit]
Description=Early Boot Service
DefaultDependencies=no
Before=sysinit.target

[Service]
Type=oneshot
ExecStart=/usr/bin/cat /proc/uptime" > /usr/lib/systemd/system/early-boot-service.service

/sbin/restorecon -v /usr/lib/systemd/system/early-boot-service.service
pushd /usr/lib/systemd/system/sysinit.target.wants/
ln -s ../early-boot-service.service

mkdir -p /usr/lib/dracut/modules.d/00early-boot-service
echo '#!/usr/bin/bash

install() {
    inst_multiple -o \
      "$systemdsystemunitdir"/early-boot-service.service \
      "$systemdsystemunitdir"/sysinit.target.wants/early-boot-service.service
}' > /usr/lib/dracut/modules.d/00early-boot-service/module-setup.sh

dracut -f
rm -f /usr/lib/systemd/system/sysinit.target.wants/early-boot-service.service

popd

if command -v abootimg &> /dev/null; then
	echo "Using abootimg..."
	abootimg_cmd $1
else
	echo "Using mkbootimg..."
	mkbootimg_cmd $1
fi



# These following commands are used for ostree images
# aboot-update $1
# aboot-deploy -d /dev/disk/by-partlabel/boot_a aboot-$1.img
# reboot

